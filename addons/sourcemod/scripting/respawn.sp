#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <t2lrcfg>
#include <warden>
#include <smlib>
//#include <jail_noblock>

#undef REQUIRE_EXTENSIONS
#include <cstrike>
#define REQUIRE_EXTENSIONS

#undef REQUIRE_PLUGIN
#include <adminmenu>
#pragma newdecls required
#define VERSION "1.8.67"
Handle hAdminMenu = null;


int g_iKiller[MAXPLAYERS+1];			// индекс убийцы
int g_iDeathTime[MAXPLAYERS+1];			// время смерти UNIX time
bool g_bWantR[MAXPLAYERS+1];			// хочет сдаться
int g_iRebelTime[MAXPLAYERS+1];			// время бунта UNIX time, если 0 знчит не бунтовал
float g_fDeathPos[MAXPLAYERS+1][3];		// позиция игрока
float g_fvPos[3];						// для телепорта
float g_fFloodTime[MAXPLAYERS+1];		// антифлуд хочет сдаться
float g_fFlood[MAXPLAYERS+1];			// антифлуд предложил сдаться
float g_fNoTime[MAXPLAYERS+1];			// Если Т отказался, то не беспокоить его N сек
bool g_bFlood[MAXPLAYERS+1];			// антифлуд (больше не хочет сдаться)
float g_fLastTP[MAXPLAYERS+1];			// антимультителепортационэйшн

public Plugin myinfo =
{
	name		= "Player Respawn and More",
	author		= "Rogue & ShaRen",
	description	= "[CS:GO] Respawn !resp !respct !cap",
	version		= VERSION,
	url			= "http://forums.alliedmods.net/showthread.php?p=984087"
}

public void OnPluginStart()
{
	RegAdminCmd("sm_respawn", Command_Respawn, ADMFLAG_SLAY, "sm_respawn <#userid|name>");
	RegConsoleCmd("sm_resp", Command_Resp);
	RegConsoleCmd("sm_respct", Command_RespCT);
	RegConsoleCmd("sm_capitulate", Command_Capitulate);
	RegConsoleCmd("sm_cap", Command_Capitulate);

	HookEvent("player_death", Event_PlayerDeath);
	HookEvent("round_start", Event_RoundStart);
	HookEvent("player_spawn", Event_PlayerSpawn);
	HookEvent("player_hurt", Event_PlayerHurt);

	Handle topmenu;
	if (LibraryExists("adminmenu") && ((topmenu = GetAdminTopMenu()) != null))
		OnAdminMenuReady(topmenu);

	LoadTranslations("common.phrases");
	LoadTranslations("respawn.phrases");
	AutoExecConfig(true, "respawn");
}

//public OnMapStart()

//public OnConfigsExecuted()

/*****************************************************************
			R E B E L   D E T E C T I O N
*****************************************************************/

public void Event_PlayerHurt(Event e, const char[] name, bool dontBroadcast)
{
	int attacker = GetClientOfUserId(e.GetInt("attacker"));
	int target = GetClientOfUserId(e.GetInt("userid"));
	char sWeapon[31];
	e.GetString("weapon", sWeapon, sizeof(sWeapon));
	// if a T attacks a CT and there's no last requests active
	if (attacker && GetClientTeam(attacker) == 2 && target && GetClientTeam(target) == 3)
		if(!(StrEqual(sWeapon, "decoy")) && !(StrEqual(sWeapon, "flashbang")) && !(StrEqual(sWeapon, "smokegrenade")))
			g_iRebelTime[attacker] = GetTime();
}

/*****************************************************************
			T E L E P O R T   F U N C T I O N S
*****************************************************************/
bool SetTeleportEndPoint(int client)
{
	float fvOrigin[3];
	float fvAngles[3];
	
	GetClientEyePosition(client, fvOrigin);
	GetClientEyeAngles(client, fvAngles);
	
	//get endpoint for teleport
	Handle trace = TR_TraceRayFilterEx(fvOrigin, fvAngles, MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
	
	if(TR_DidHit(trace)) {   
		float fvBuffer[3];
		float fvStart[3];
		TR_GetEndPosition(fvStart, trace);
		GetVectorDistance(fvOrigin, fvStart, false);
		float fDistance = -55.0;
		GetAngleVectors(fvAngles, fvBuffer, NULL_VECTOR, NULL_VECTOR);
		g_fvPos[0] = fvStart[0] + (fvBuffer[0]*fDistance);
		g_fvPos[1] = fvStart[1] + (fvBuffer[1]*fDistance);
		g_fvPos[2] = fvStart[2] + (fvBuffer[2]*fDistance);
		CloseHandle(trace);
		return true;
	} else {
		PrintToChat(client, "[SM] Не удалось телепортировать игрока");
		CloseHandle(trace);
		return false;
	}
}

public bool TraceEntityFilterPlayer(int entity, int contentsMask)
{
	return entity > GetMaxClients() || !entity;
}


stock void StripAllWeapons(int client)
{
	int wepIdx;
	for (int i=0; i<4; i++)
		if ((wepIdx = GetPlayerWeaponSlot(client, i)) != -1 && i!=2) {			// && i!=2 #define CS_SLOT_KNIFE	2	/**< Knife slot. */
			//PrintToChatAll("%i wepIdx = %i client = %i", i, wepIdx, client);
			RemovePlayerItem(client, wepIdx);
			AcceptEntityInput(wepIdx, "Kill");
		}
}

/*****************************************************************
			M E N U S
*****************************************************************/
public Action Command_Capitulate(int iClient, int args)
{
	if ( !IsPlayerAlive(iClient)) {
		PrintToChat(iClient, "Вы должны быть живым");
		return Plugin_Handled;
	}
	if (IsLrActivated()) {
		PrintToChat(iClient, "В ЛР !refuse не работает");
		return Plugin_Handled;
	}
	int iTeam = GetClientTeam(iClient);
	if (iTeam == 3) {
		Menu menu = CreateMenu(CapMenuHandler);
		menu.SetTitle("Предложить сдаться бунтующим/n (или где-то застрявшим)");
		char sIt[3], sName[64];
		float fTime;
		for(int i=1; i<=MaxClients; i++)
			if(g_bWantR[i] && IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == 2) {
				IntToString(i, sIt, sizeof(sIt));
				GetClientName(i, sName, sizeof(sName));
				if ((fTime = GetGameTime() - g_fLastTP[i]) > 40.0) {
					Format(sName, sizeof(sName), "%N (Хочет сдаться) [Телепортировать к себе (по прицелу)]", i);
					menu.AddItem(sIt, sName);
				} else {
					Format(sName, sizeof(sName), "%N (Хочет сдаться) [подождите %.1f сек]", i, 40.0 - fTime);
					menu.AddItem(sIt, sName, ITEMDRAW_DISABLED);
				}
			}
		for(int i=1; i<=MaxClients; i++)
			if(IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == 2 && !g_bWantR[i]) {
				IntToString(i, sIt, sizeof(sIt));
				GetClientName(i, sName, sizeof(sName));
				if ((fTime = GetGameTime() - g_fLastTP[i]) > 40.0) {
					if ((fTime = GetGameTime() - g_fNoTime[i]) > 40.0) {
						Format(sName, sizeof(sName), "%N [предложить сдаться]", i);
						menu.AddItem(sIt, sName);
					} else {
						Format(sName, sizeof(sName), "%N (отказался сдаваться) [подождите %.1f сек]", i, 40.0 - fTime);
						menu.AddItem(sIt, sName, ITEMDRAW_DISABLED);
					}
				} else {
					Format(sName, sizeof(sName), "%N (недавно сдался) [подождите %.1f сек]", i, 40.0 - fTime);
					menu.AddItem(sIt, sName, ITEMDRAW_DISABLED);
				}
			}
		menu.ExitButton = true;
		menu.Display(iClient, 20);
	} else if (iTeam == 2) {
		if (!g_bWantR[iClient]) {
			g_bWantR[iClient] = true;
			PrintToChat(iClient, "Ваше предложение отправлено");
			if(GetGameTime() - g_fFloodTime[iClient] > 20.0) {
				g_fFloodTime[iClient] = GetGameTime();
				g_bFlood[iClient] = true;
				for (int i=1; i<=MaxClients; i++)
					if (IsClientInGame(i) && GetClientTeam(i) == 3 && IsPlayerAlive(i)) {
						PrintToChat(i, "%N хочет сдаться, напишите !cap чтобы подтвердить. (или через !w мнею)", iClient);
						PrintHintText(i, "%N хочет сдаться", iClient);
					}
			}
		} else {
			g_bWantR[iClient] = false;
			PrintToChat(iClient, "Вы больше не хотите сдаться");
			if(g_bFlood[iClient]) {
				g_bFlood[iClient] = false;
				for (int i=1; i<=MaxClients; i++)
					if (IsClientInGame(i) && GetClientTeam(i) == 3 && IsPlayerAlive(i))
						PrintToChat(i, "%N больше не хочет сдаться", iClient);
			}
		}
	}

	return Plugin_Continue;
}

public int CapMenuHandler(Menu menu, MenuAction action, int iClient, int itemNum) 
{
	if (action == MenuAction_Select)
		if (IsPlayerAlive(iClient))
			if (GetClientTeam(iClient) == 3) {
				if (!IsLrActivated()) {
					char sIt[3];
					menu.GetItem(itemNum, sIt, sizeof(sIt));
					int iT = StringToInt(sIt);
					if (!g_bWantR[iT]) {
						char sI[3];
						IntToString(iClient, sI, sizeof(sI));
						Menu menuT = CreateMenu(CapMenuHandlerT);
						menuT.SetTitle("Вам предложили сдаться\nСдавшись вы телепортируетесь к КТ\nОружие автоматически удалится\nВсе прошлые бунты убивать не будут");
						menuT.AddItem("0", "Не сдаваться");
						menuT.AddItem(sI, "Сдаться");
						menuT.ExitButton = true;
						menuT.Display(iT, 20);
						if(GetGameTime() - g_fFlood[iT] > 10.0) {
							g_fFlood[iT] = GetGameTime();
							PrintToChatAll("%N предложил сдаться %N", iClient, iT);
						}
					} else {
						if(IsClientInGame(iT) && IsPlayerAlive(iT) && SetTeleportEndPoint(iClient)) {
							TeleportEntity(iT, g_fvPos, NULL_VECTOR, NULL_VECTOR);
							//UnStack(iT);
							g_bWantR[iT] = false;
							StripAllWeapons(iT);
							PrintToChatAll("%N сдался %N", iT, iClient);
						}
					}
					Command_Capitulate(iClient, 0);
				} else PrintToChat(iClient, "В ЛР !cap не работает");
			} else PrintToChat(iClient, "Только охранники могут предлогать сдаваться");
		else PrintToChat(iClient, "Вы должны быть живым");
	else if (action == MenuAction_End)
		delete menu;
}

public int CapMenuHandlerT(Menu menuT, MenuAction action, int iClient, int itemNum) 
{
	if (action == MenuAction_Select)
		if (IsPlayerAlive(iClient))
			if (GetClientTeam(iClient) == 2) {
				if (!IsLrActivated()) {
					if (itemNum) {
						g_bWantR[iClient] = true;		// если вдруг КТ закроет меню
						char sI[3];
						menuT.GetItem(itemNum, sI, sizeof(sI));			// КТ кто предложил
						int iCT = StringToInt(sI);
						IntToString(iClient, sI, sizeof(sI));
						Menu menuCT = CreateMenu(CapMenuHandlerCT);
						menuCT.SetTitle("Телепортировать игрока:");
						menuCT.AddItem(sI, "К себе (по прицелу)");		// тут sI это Т т.е. iClient
						//menuCT.AddItem(sI, "В джайл");
						menuCT.ExitButton = true;
						menuCT.Display(iCT, 20);
						PrintToChatAll("%N согласился сдаться", iClient);
					} else {
						PrintToChatAll("%N не согласился сдаться", iClient);
						PrintToChat(iClient, "Если вы передумаете и захотите сдаться, то напишите !cap");
						g_fNoTime[iClient] = GetGameTime();
					}
				} else PrintToChat(iClient, "В ЛР !cap не работает");
			} else PrintToChat(iClient, "Только охранники могут предлогать сдаваться");
		else PrintToChat(iClient, "Вы должны быть живым");
	else if (action == MenuAction_End)
		delete menuT;
}

public int CapMenuHandlerCT(Menu menuCT, MenuAction action, int iClient, int itemNum) 
{
	if (action == MenuAction_Select)
		if (IsPlayerAlive(iClient))
			if (GetClientTeam(iClient) == 3) {
				if (!IsLrActivated()) {
					char sI[3];
					menuCT.GetItem(itemNum, sI, sizeof(sI));			// Т который сдается
					int iT = StringToInt(sI);
					if(IsClientInGame(iT) && IsPlayerAlive(iT) && SetTeleportEndPoint(iClient)) {
						TeleportEntity(iT, g_fvPos, NULL_VECTOR, NULL_VECTOR);
						//UnStack(iT);
						g_bWantR[iT] = false;
						StripAllWeapons(iT);
						PrintToChatAll("%N сдался %N", iT, iClient);
					}
					Command_Capitulate(iClient, 0);
				} else PrintToChat(iClient, "В ЛР !cap не работает");
			} else PrintToChat(iClient, "Только охранники могут предлогать сдаваться");
		else PrintToChat(iClient, "Вы должны быть живым");
	else if (action == MenuAction_End)
		delete menuCT;
}

public Action Command_Resp(int iClient, int args)
{
	if (GetClientTeam(iClient) != 3 || !IsPlayerAlive(iClient)) {
		PrintToChat(iClient, "Вы должны быть живым охранником");
		return Plugin_Handled;
	}
	if (IsLrActivated()) {
		PrintToChat(iClient, "В ЛР !resp не работает");
		return Plugin_Handled;
	}
	Menu menu = CreateMenu(RespMenuHandler);
	menu.SetTitle("Возродить игроков,/nубитых на линии огня или случайно");
	int iCount, iTime[MAXPLAYERS+1], iDead[MAXPLAYERS+1];
	// iCount сколько Т убил iClient; iTime - сколько секунд назад умерли Т; iDead убитые клиентом Т
	for(int i=1; i<=MaxClients; i++) {
		if(IsPlayerValid(i) && GetClientTeam(i) == 2 && g_iDeathTime[i] && (g_iKiller[i] == iClient || !g_iKiller[i])) {
		// если i это Т && если i умер && (если i убил iClient или его никто не убивал)
			iTime[i] = GetTime() - g_iDeathTime[i];
			iDead[iCount] = i;		// запишем убитых Т клиентом в массив
			iCount++;
		} else iTime[i] = -1;
	}
	if(!iCount) {
		PrintToChat(iClient, "Вы никого не убивали.");
		return Plugin_Continue;
	}
	for(int i=0, iTemp; i<iCount-1; i++)		// сортируем убитых игроков чтобы в начале списка шли кто раньше умер
		for(int j=i+1; j<iCount; j++)
			if (iTime[iDead[i]] != -1 && iTime[iDead[i]] < iTime[iDead[j]]) {
				iTemp = iDead[i];
				iDead[i] = iDead[j];
				iDead[j] = iTemp;
			}
	char sI[3], sName[MAXPLAYERS][64], sRebel[64];
	for(int i=iCount-1; i>=0; i--) {
		IntToString(iDead[i], sI, sizeof(sI));
		GetClientName(iDead[i], sName[iDead[i]], sizeof(sName[]));
		if (0 <= g_iDeathTime[iDead[i]] - g_iRebelTime[iDead[i]] < 40)
			Format (sRebel, sizeof(sRebel[]), "[бунт - i% сек назад]", GetTime() - g_iRebelTime[iDead[i]]);
		else Format (sRebel, sizeof(sRebel[]), "");
		Format(sName[iDead[i]], sizeof(sName[]), "%s%s%s умер %i сек назад%s", sRebel, !g_iKiller[iDead[i]]?" [PropKill]":"", iTime[iDead[i]], !g_iKiller[iDead[i]]?", (по прицелу)":"");
		menu.AddItem(sI, sName[iDead[i]]);
	}
	menu.ExitButton = true;
	menu.Display(iClient, 20);

	return Plugin_Continue;
}

public int RespMenuHandler(Menu menu, MenuAction action, int iClient, int itemNum) 
{
	if (action == MenuAction_Select) {
		if (GetClientTeam(iClient) == 3 && IsPlayerAlive(iClient)) {
			if (!IsLrActivated()) {
				char info[3];
				menu.GetItem(itemNum, info, sizeof(info));
				int iTarget = StringToInt(info);
				int iKiller = g_iKiller[iTarget];		// т.к. после RespawnPlayer g_iKiller обнуляется
				RespawnPlayer(iClient, iTarget);
				if (iKiller) {
					g_fDeathPos[iTarget][2] -= 63.0;
					TeleportEntity(iTarget, g_fDeathPos[iTarget], NULL_VECTOR, NULL_VECTOR);
				} else if (SetTeleportEndPoint(iClient)) {
					TeleportEntity(iTarget, g_fvPos, NULL_VECTOR, NULL_VECTOR);
				}
				Command_Resp(iClient, 0);
			} else PrintToChat(iClient, "В ЛР !resp не работает");
		} else PrintToChat(iClient, "Вы должны быть живым охранником");
	} else if (action == MenuAction_End)
		delete menu;
}

public Action Command_RespCT(int iClient, int args)
{
	if (!warden_iswarden(iClient)) {
		PrintToChat(iClient, "Вы должны быть КМД");
		return Plugin_Handled;
	}
	if (!IsPlayerAlive(iClient)) {
		PrintToChat(iClient, "Вы должны быть живы");
		return Plugin_Handled;
	}
	if (IsLrActivated()) {
		PrintToChat(iClient, "В ЛР !respct не работает");
		return Plugin_Handled;
	}
	int iCTs, iTs;
	Team_GetAliveClientCounts(iCTs, iTs);
	if (iTs/iCTs < 6.1) {
		PrintToChat(iClient, "Слошком мало живых Т");
		return Plugin_Handled;
	}
	Handle menu = CreateMenu(RespCTMenuHandler);
	SetMenuTitle(menu, "Подкрепление. Возродить КТ");
	int count;
	int iTime[MAXPLAYERS+1];
	int iDead[32];
	for (int i=1; i<MAXPLAYERS; i++) {
		if (IsPlayerValid(i) && GetClientTeam(i) == 3 && g_iDeathTime[i]) {
			int iDeday = GetTime() - g_iDeathTime[i];
			if (iDeday > 70) {
				iDead[count] = i;
				count++;
			} else {
				PrintToChat(iClient, "С момента последней смерти КТ должно пройти 70 секунд (прошло только %i)", iDeday);
				return Plugin_Handled;
			}
		}
	}
	if(!count) {
		PrintToChat(iClient, "Нету подходящих КТ.");
		return Plugin_Continue;
	}
	for(int i=0, temp; i<count-1; i++)
		for(int j=i+1; j<count; j++)
			if (iDead[i] < iDead[j]) {
				temp = iDead[i];
				iDead[i] = iDead[j];
				iDead[j] = temp;
			}
	
	char sI[3], sName[MAXPLAYERS][64];
	for(int i=count-1; i>=0; i--) {
		IntToString(iDead[i], sI, sizeof(sI));
		GetClientName(iDead[i], sName[iDead[i]], sizeof(sName[]));
		Format(sName[iDead[i]], sizeof(sName[]), "%s %i сек назад", sName[iDead[i]], iTime[iDead[i]]);
		AddMenuItem(menu, sI, sName[iDead[i]]);
	}
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, iClient, 20);

	return Plugin_Continue;
}

public int RespCTMenuHandler(Handle menu, MenuAction action, int iClient, int itemNum) 
{
	if ( action == MenuAction_Select ) {
		if (warden_iswarden(iClient)) {
			if (IsPlayerAlive(iClient)) {
				if (IsLrActivated()) {
					char info[3];
					GetMenuItem(menu, itemNum, info, sizeof(info));
					int target = StringToInt(info);
					RespawnPlayer(iClient, target);
					PrintToChatAll("[!respCT] КМД %N возродил %N", iClient, target);
				} else PrintToChat(iClient, "В ЛР !respct не работает");
			} else PrintToChat(iClient, "Вы должны быть живы");
		} else PrintToChat(iClient, "Вы должны быть КМД");
	} else if (action == MenuAction_End)
		CloseHandle(menu);
}

public Action Command_Respawn(int client, int args)
{
	if (args < 1) {
		ReplyToCommand(client, "[SM] Usage: sm_respawn <#userid|name>");
		return Plugin_Handled;
	}

	char arg[65];
	GetCmdArg(1, arg, sizeof(arg));

	char target_name[MAX_TARGET_LENGTH];
	int target_list[MAXPLAYERS], target_count;
	bool tn_is_ml;

	target_count = ProcessTargetString(
					arg,
					client,
					target_list,
					MaxClients,
					COMMAND_FILTER_CONNECTED|COMMAND_FILTER_DEAD,
					target_name,
					sizeof(target_name),
					tn_is_ml);


	if(target_count <= COMMAND_TARGET_NONE) {		// If we don't have dead players
		ReplyToTargetError(client, target_count);
		return Plugin_Handled;
	}

	// Team filter dead players, re-order target_list array with new_target_count
	int target, team, new_target_count;

	for (int i=0; i<target_count; i++) {
		target = target_list[i];
		team = GetClientTeam(target);

		if(team >= 2) {
			target_list[new_target_count] = target; // re-order
			new_target_count++;
		}
	}

	if(new_target_count == COMMAND_TARGET_NONE) { // No dead players from  team 2 and 3
		ReplyToTargetError(client, new_target_count);
		return Plugin_Handled;
	}

	target_count = new_target_count; // re-set new value.

	if (tn_is_ml)
		ShowActivity2(client, "[SM] ", "%t", "Toggled respawn on target", target_name);
	else ShowActivity2(client, "[SM] ", "%t", "Toggled respawn on target", "_s", target_name);

	for (int i=0; i < target_count; i++)
		RespawnPlayer(client, target_list[i]);

	return Plugin_Handled;
}

public void Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
	for(int i=0; i<MAXPLAYERS; i++) {
		g_iKiller[i] = 0;
		g_bWantR[i] = false;
	}
}

public void Event_PlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	int attacker = GetClientOfUserId(event.GetInt("attacker"));
	if (client != attacker && client && attacker && GetClientTeam(attacker) == 3) {
		if (GetTime() - g_iRebelTime[client] < 30)
			PrintToChat(attacker, "Пиши !resp если случайно убил игрока %N, чтобы возродить его.", client);
		g_iKiller[client] = attacker;
	}
	g_iDeathTime[client] = GetTime();
	GetEntPropVector(client, Prop_Send, "m_vecOrigin", g_fDeathPos[client]);
}

public void Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	g_iKiller[client] = 0;
	g_iDeathTime[client] = 0;
	g_iRebelTime[client] = 0;
}

public void RespawnPlayer(int client, int target)
{
	if (target && IsClientInGame(target)) {
		LogAction(client, target, "\"%L\" respawned \"%L\"", client, target);
		CS_RespawnPlayer(target);
	}
}

public void OnLibraryRemoved(const char[] name)
{
	if (StrEqual(name, "adminmenu"))
		hAdminMenu = null;
}

public void OnAdminMenuReady(Handle topmenu)
{
	if (topmenu == hAdminMenu)
		return;
	
	hAdminMenu = topmenu;

	TopMenuObject player_commands = FindTopMenuCategory(hAdminMenu, ADMINMENU_PLAYERCOMMANDS);

	if (player_commands != INVALID_TOPMENUOBJECT) {
		AddToTopMenu(hAdminMenu,
		"sm_respawn",
		TopMenuObject_Item,
		AdminMenu_Respawn,
		player_commands,
		"sm_respawn",
		ADMFLAG_SLAY);
	}
}

public void AdminMenu_Respawn( Handle topmenu, TopMenuAction action, TopMenuObject object_id, int param, char[] buffer, int maxlength )
{
	if (action == TopMenuAction_DisplayOption)
		Format(buffer, maxlength, "Respawn Player");
	else if( action == TopMenuAction_SelectOption)
		DisplayPlayerMenu(param);
}

void DisplayPlayerMenu(int client)
{
	Menu menu = CreateMenu(MenuHandler_Players);

	char title[64];
	Format(title, sizeof(title), "Choose Player to Respawn:");
	menu.SetTitle(title);
	menu.ExitBackButton = true;

	// AddTargetsToMenu(menu, client, true, false);
	// Lets only add dead players to the menu... we don't want to respawn alive players do we?
	AddTargetsToMenu2(menu, client, COMMAND_FILTER_DEAD);

	menu.Display(client, MENU_TIME_FOREVER);
}

public int MenuHandler_Players(Handle menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_End)
		CloseHandle(menu);
	else if (action == MenuAction_Cancel) {
		if (param2 == MenuCancel_ExitBack && hAdminMenu != null)
			DisplayTopMenu(hAdminMenu, param1, TopMenuPosition_LastCategory);
	} else if (action == MenuAction_Select) {
		int userid, target;
		char info[32];

		GetMenuItem(menu, param2, info, sizeof(info));
		userid = StringToInt(info);

		if (!(target = GetClientOfUserId(userid)) )
			PrintToChat(param1, "[SM] %t", "Player no longer available");
		else if (!CanUserTarget(param1, target))
			PrintToChat(param1, "[SM] %t", "Unable to target");
		else {
			char name[32];
			GetClientName(target, name, sizeof(name));

			RespawnPlayer(param1, target);
			ShowActivity2(param1, "[SM] ", "%t", "Toggled respawn on target", "_s", name);
		}

		/* Re-draw the menu if they're still valid */
		if (IsClientInGame(param1) && !IsClientInKickQueue(param1))
			DisplayPlayerMenu(param1);
	}
}

stock bool IsPlayerValid(int client)
{
	if (client <= 0 || client >= 65 || !IsClientInGame(client) || IsPlayerAlive(client))// || IsFakeClient(client)
	//if (client <= 0 || client >= 65 || !IsClientInGame(client) || !IsPlayerAlive(client) ||  IsFakeClient(client))
		return false;
	return true;
}